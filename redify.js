var Jimp = require('jimp')
  , args = require('minimist')(process.argv.slice(3), {string: ["red", "blue", "green"]})
  , PATH = process.argv[2].indexOf('/') === -1 ? 'images/' : process.argv[2].substring(0, process.argv[2].lastIndexOf('/'))
  , FILENAME = process.argv[2].indexOf('/') === -1 ? 'images/' + process.argv[2] : process.argv[2]
  , OUTFILE = FILENAME.indexOf('.png') !== -1 ? FILENAME.replace('.png', '-mod.png') : FILENAME.replace('.jpg', '-mod.jpg')
  , blank = 'blank'
  , red = args.red || blank
  , blue = args.blue || blank
  , green = args.green || blank
  ;

console.log('redifying ' + FILENAME);

Jimp.read(FILENAME).then(function (viking) {
  var colors = [red, green, blue];
  console.log(colors);
  viking.scan(0, 0, viking.bitmap.width, viking.bitmap.height, function (x, y, index) {
    // var increase = Math.floor((255 - this.bitmap.data[i]) / 2)

    for (var i = 0, l = colors.length; i < l; i++) {
      if (colors[i] !== blank) {
        switch (colors[i]) {
          case 'all':
            this.bitmap.data[index + i] = 255;
            break;
          case 'none':
            this.bitmap.data[index + i] = 0;
            break;
          case 'more':
            this.bitmap.data[index + i] += Math.floor((255 - this.bitmap.data[index + i]) / 2);
            break;
          case 'less':
            this.bitmap.data[index + i] = Math.floor(this.bitmap.data[index + i] / 2);
            break;
          default:
            break;
        }
      }

    }
    // var red   = this.bitmap.data[ i + 0 ];
    // var green = this.bitmap.data[ i + 1 ];
    // var blue  = this.bitmap.data[ i + 2 ];
    // var alpha = this.bitmap.data[ i + 3 ];
  }).write(OUTFILE, function () {
    console.log(OUTFILE + ' written');
  });
}).catch(function (error) {
  console.log(error);
});
